public class MAZE {
    bool barriers[][];
	int seeds[][];
	int distance[][];
	int width;
	int depth;

    void MAZE(int width, int depth) {
		this.width = width;
		this.depth = depth;

        for(int i = 0; i < width; i++) {
            for(int j = 0; j < depth; j++) {
                barriers[i][j] = true;
				seeds[i][j] = 0;
				distance[i][j] = 0;
            }
        }
	}
	
	void ghostRide() {

		int xxx = 1,
			yyy = 1;
		
		int rotation = 0;
		
		while(xxx < maze.width && yyy < maze.depth)
		{
			if(rotation == 0 || rotation == 360 || rotation == -360)
			{
				rotation = 0;
				if(maze.barriers[xxx + 1][yyy] == true)
				{
					maze.seeds[xxx][yyy] = 1;
					rotation -= 90; 
					xxx += 2;
				}
				else if(maze.barriers[xxx][yyy + 1] == true)
				{
					maze.seeds[xxx][yyy] = 2;
					yyy += 2;
				}
				else if(maze.barriers[xxx - 1][yyy] == true)
				{
					maze.seeds[xxx][yyy] = 3;
					rotation += 90;
					xxx -= 2;
				}
				else if(maze.barriers[xxx][yyy - 1] == true)
				{
					maze.seeds[xxx][yyy] = 4;
					rotation += 180;
					yyy -= 2;
				}
				continue;
			}
			
			if(rotation  == -90 || rotation  == 270 || rotation  == -430)
			{
				rotation = -90;
				if(maze.barriers[xxx][yyy - 1] == true)
				{
					maze.seeds[xxx][yyy] = 4;
					rotation -= 90;
					yyy -= 2;
				}
				else if(maze.barriers[xxx + 1][yyy] == true)
				{
					maze.seeds[xxx][yyy] = 1;
					xxx += 2;
				}
				else if(maze.barriers[xxx][yyy + 1] == true)
				{
					maze.seeds[xxx][yyy] = 2;
					rotation += 90;
					yyy += 2;
				}
				else if(maze.barriers[xxx - 1][yyy] == true)
				{
					maze.seeds[xxx][yyy] = 3;
					rotation += 180;
					xxx -= 2;
				}
				continue;
			}
			
			if(rotation  == 90 || rotation  == -270 || rotation  == 430)
			{
				rotation = 90;
				if(maze.barriers[xxx][yyy + 1] == true)
				{
					maze.seeds[xxx][yyy] = 2;
					rotation -= 90;
					yyy += 2;
				}
				else if(maze.barriers[xxx - 1][yyy] == true)
				{
					maze.seeds[xxx][yyy] = 3;
					xxx -= 2;
				}
				else if(maze.barriers[xxx][yyy - 1] == true)
				{
					maze.seeds[xxx][yyy] = 4;
					rotation += 90;
					yyy -= 2;
				}
				else if(maze.barriers[xxx + 1][yyy] == true)
				{
					maze.seeds[xxx][yyy] = 1;
					rotation += 180;
					xxx += 2;
				}
				continue;
			}
			
			if(rotation  == 180 || rotation  == -180 || rotation  == 540 || rotation  == -540)
			{
				rotation = 180;
				if(maze.barriers[xxx - 1][yyy] == true)
				{
					maze.seeds[xxx][yyy] = 3;
					rotation -= 90;
					xxx -= 2;
				}
				else if(maze.barriers[xxx][yyy - 1] == true)
				{
					maze.seeds[xxx][yyy] = 4;
					yyy -= 2;
				}
				else if(maze.barriers[xxx + 1][yyy] == true)
				{
					maze.seeds[xxx][yyy] = 1;
					rotation += 90;
					xxx += 2;
				}
				else if(maze.barriers[xxx][yyy + 1] == true)
				{
					maze.seeds[xxx][yyy] = 2;
					rotation += 180;
					yyy += 2;
				}
				continue;
			}
			
		}
	}
}

public class Measurements {
    float getWidth(float dist) {           //------Moves given distance to front and measures WIDTH of the maze
		move(dist);
		
		object left = radar(Barrier, 90, 1, 0, 400, -1),
			   right = radar(Barrier, -90, 1, 0, 400, -1);

        return distance(left.position, right.position);
    }

    object findFurthestBarrier(point position) {           //-----Measures DEPTH of the maze
		object front1 = radar(Barrier, 0, 1, 0, 400, -1),
			   front2 = search(Barrier, front1.position.x + 10, front1.position.y + 50);  //Including possibility when exit is in front
  
        if(position.y + front1.position.y >= position.y + front1.position.y) {
                return front1;
            } else {
                return front2;
            }
    }
        
    float addjustToGrid(float distance, float addition) {
        return (distance + addition) / 5  * 2 + 1;
	}

	void searchBarriersVertically(MAZE maze, point position) {
		object left = search(Barrier, position.x - 500, position.y),
			   barrier;
		float searchX,
			  searchY;
		int xxx = 0,
			yyy = 1;

		while(yyy < maze.depth - 1)
		{
			xxx = 0;
			while(xxx <= maze.width)
			{
				searchX = left.position.x + xxx / 2 * 5; 
				searchY = position.y + (yyy - 1) / 2 * 5;

				barrier = search(Barrier, searchX, searchY);

				if(barrier.position.x == searchX && barrier.position.y == searchY)
				{
					maze.barriers[xxx][yyy + 1] = false;
					maze.barriers[xxx][yyy] = false;
					maze.barriers[xxx][yyy - 1] = false;
				}
				xxx += 2;
			}
			yyy += 2;
		}
	}

	void searchBarriersHorizontally(MAZE maze, point position) {
		object barrier;
		float searchX,
			  searchY;
		int xxx = 1,
			yyy = 0;
	
		while(yyy <= maze.depth)
		{
			xxx = 1;
			while(xxx < maze.width - 1)
			{
				searchX = position.x + (xxx - 1) / 2 * 5;
				searchY = position.y + yyy / 2 * 5 - 2.5;

				barrier = search(Barrier, searchX, searchY);

				if(barrier.position.x == searchX && barrier.position.y == searchY)
				{
						maze.barriers[xxx+1][yyy] = false;
						maze.barriers[xxx][yyy] = false;
						maze.barriers[xxx-1][yyy] = false;
				}
				xxx += 2;
			}
			yyy += 2;
		}
	}
}

extern void object::conceptOfFinal()
{
    Measurements measure = new Measurements;;
	
	MAZE maze = new MAZE(
		measure.addjustToGrid(
			measure.getWidth(5),
			0
		),
		measure.addjustToGrid(
			distance(
				position, 
				measure.findFurthestBarrier(position).position
			),
			2.5)
		);

	maze.barriers[1][0] = false;

    measure.searchBarriersVertically(maze, position);
    measure.searchBarriersHorizontally(maze, position);
	
    //--------------------------------------------LEFT-END-OF-MAZE-----------------------------------------
    
    

	//--------------------------------------SEARCHING BARRIERS VERTICALY-----------------------------------

	
	
    //--------------------------------------SEARCHING BARRIERS HORIZONTALY---------------------------------


	//-------------------------------------------SAVING MAP TO FILE---------------------------------------

	// int m = 0, b = 0;

	// file map();
	// map.open("mapOfMaze.txt", "w");
	// while(m < maze.depth)
	// {
	// 	b=0;
	// 	while(b < maze.width)
	// 	{
	// 		if(maze.barriers[b][m]==true)
	// 		{
	// 			map.writeln("h");
	// 		}
	// 		if(maze.barriers[b][m]==false)
	// 		{
	// 			map.writeln("0");
	// 		}
	// 		b++;
	// 	}
	// 	m++;
	// }
	// map.close();

    //----------------------------------------------------------------------------------------------------------------------------------------------------------UPPER PART IS FINISHED
    //----------------------------------------------------------------------------------------------------------------------------------------------------------UPPER PART IS FINISHED
    //----------------------------------------------------------------------------------------------------------------------------------------------------------UPPER PART IS FINISHED

	float wx, wy;
	object the_end_is_where_we_begin = radar(GoalArea, 0, 360, 0, 300, -1);
	// if(the_end_is_where_we_begin == null) {
	// 	message("Well it's kinda fucked");
	// } else {
	// 	message(the_end_is_where_we_begin.position.x);
	// 	message(the_end_is_where_we_begin.position.y);
	// }
	
	
	
	//---------------------------------------------------------------------------------------------------------symulacja jazdy wewnatrz maze.barriersu


	int kont=0;
	yyy = 1;
	xxx = 1;
	wx = abs(position.x - the_end_is_where_we_begin.position.x) / 5 * 2;
	if(wx == 0) {
		wx = xxx;
	}
	wy = abs(position.y - the_end_is_where_we_begin.position.y) / 5 * 2 - 1;

	while(xxx != wx || yyy != wy)
	{
		if(xxx >= maze.width || yyy >= maze.depth) break;
		kont=0;
			 if(maze.seeds[xxx][yyy] == 1) {while(maze.seeds[xxx][yyy] == 1) { xxx += 2; kont++; if(xxx < maze.width && yyy < maze.depth) {continue;} else {break;} } maze.distance[xxx - (kont * 2)][yyy] = kont;}
		else if(maze.seeds[xxx][yyy] == 2) {while(maze.seeds[xxx][yyy] == 2) { yyy += 2; kont++; if(xxx < maze.width && yyy < maze.depth) {continue;} else {break;} } maze.distance[xxx][yyy - (kont * 2)] = kont;}
		else if(maze.seeds[xxx][yyy] == 3) {while(maze.seeds[xxx][yyy] == 3) { xxx -= 2; kont++; if(xxx < maze.width && yyy < maze.depth) {continue;} else {break;} } maze.distance[xxx + (kont * 2)][yyy] = kont;}
		else if(maze.seeds[xxx][yyy] == 4) {while(maze.seeds[xxx][yyy] == 4) { yyy -= 2; kont++; if(xxx < maze.width && yyy < maze.depth) {continue;} else {break;} } maze.distance[xxx][yyy + (kont * 2)] = kont;}
	}
	
	
	
	
	yyy = 1;
	xxx = 1;
	
	
	int dist;
	rotation = 0;
	while(xxx != wx || yyy != wy)
	{
		dist = 5 * maze.distance[xxx][yyy];
		
		if(rotation  == 0 || rotation  == 360 || rotation  == -360)
		{
			rotation = 0;
			if(maze.seeds[xxx][yyy] == 1)
			{
				turn(-90);
				rotation -= 90; 
				xxx += 2 * maze.distance[xxx][yyy];
				move(dist);
				continue;
			}
			else if(maze.seeds[xxx][yyy] == 2)
			{
			 	yyy += 2 * maze.distance[xxx][yyy];
			  	move(dist);
				continue;
			}
			else if(maze.seeds[xxx][yyy] == 3)
			{
				turn(90);
				rotation = 90;
				xxx -= 2 * maze.distance[xxx][yyy];
				move(dist);
				continue;
			}
			else if(maze.seeds[xxx][yyy] == 4)
			{
				turn(-180);
				rotation += 180;
				yyy -= 2 * maze.distance[xxx][yyy];
				move(dist);
				continue;
			}
			continue;
		}
		
		if(rotation  == -90 || rotation  == 270 || rotation  == -430)
		{
			rotation =-90;
			if(maze.seeds[xxx][yyy] == 4)
			{
				turn(-90); rotation -=90; yyy-=2*maze.distance[xxx][yyy]; move(dist); continue;
			}
			else if(maze.seeds[xxx][yyy] == 1)
			{
			 xxx+=2*maze.distance[xxx][yyy]; move(dist); continue;
			}
			else if(maze.seeds[xxx][yyy] == 2)
			{
				turn(90); rotation +=90; yyy+=2*maze.distance[xxx][yyy]; move(dist); continue;
			}
			else if(maze.seeds[xxx][yyy] == 3)
			{
				turn(-180); rotation +=180; xxx-=2*maze.distance[xxx][yyy]; move(dist); continue;
			}
			continue;
		}
		
		if(rotation  == 90 || rotation  == -270 || rotation  == 430)
		{
			rotation =90;
			if(maze.seeds[xxx][yyy] == 2)
			{
				turn(-90); rotation -=90; yyy+=2*maze.distance[xxx][yyy]; move(dist); continue;
			}
			else if(maze.seeds[xxx][yyy] == 3)
			{
			 xxx-=2*maze.distance[xxx][yyy]; move(dist); continue;
			}
			else if(maze.seeds[xxx][yyy] == 4)
			{
				turn(90); rotation +=90; yyy-=2*maze.distance[xxx][yyy]; move(dist); continue;
			}
			else if(maze.seeds[xxx][yyy] == 1)
			{
				turn(-180); rotation +=180; xxx+=2*maze.distance[xxx][yyy]; move(dist); continue;
			}
			continue;
		}
		
		if(rotation  == 180 || rotation  == -180 || rotation  == 540 || rotation  == -540)
		{
			rotation =180;
			if(maze.seeds[xxx][yyy] == 3)
			{
				turn(-90); rotation -=90; xxx-=2*maze.distance[xxx][yyy]; move(dist); continue;
			}
			else if(maze.seeds[xxx][yyy] == 4)
			{
			 yyy -= 2*maze.distance[xxx][yyy]; move(dist); continue;
			}
			else if(maze.seeds[xxx][yyy] == 1)
			{
				turn(90); rotation +=90; xxx+=2*maze.distance[xxx][yyy]; move(dist); continue;
			}
			else if(maze.seeds[xxx][yyy] == 2)
			{
				turn(-180); rotation +=180; yyy+=2*maze.distance[xxx][yyy]; move(dist); continue;
			}
			continue;
		}
		
	}
}
